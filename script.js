const state = {
    comments: [],
    isAlertEnabled: false
}
function formatDate(data){
    const options = {
        year: "numeric",
        month: "long",
        day: "numeric",
        hour: "numeric",
        minute: "numeric"
    };
    return new Date(data).toLocaleString("ru", options);
}
function renderValidationAlert(){
    if (state.isAlertEnabled){
        return
    }
    let el = document.getElementById('comment-box');
    let el2 = document.createElement('div');
    el2.classList.add('alert');
    el2.id = 'alert';
    el2.textContent = '️Заполните все поля(имя, комментарий)';
    el.appendChild(el2);
    state.isAlertEnabled = true
}
function hideValidationAlert(){
    if (state.isAlertEnabled){
        let container = document.getElementById('comment-box');
        let el = document.getElementById('alert');
        container.removeChild(el);
        state.isAlertEnabled = false
    }
}
function onButtonClick(){
    let name = document.getElementById('name').value;
    let text = document.getElementById('text').value;
    let date = document.getElementById('date-box').valueAsDate;
    if (text.trim() === '' || name.trim() === ''){
        renderValidationAlert()
        return;
    }
    let comment = {
        name: name,
        text: text,
        data: date === null ? new Date(): date,
        like: false
    }
    state.comments.push(comment);
    render();
}
function renderComment(comment, id){
    let newComment = document.createElement('div');
    newComment.classList.add('new-comment-box');
    newComment.id = id;
    let newCommentBox1 = document.createElement('div');
    newCommentBox1.classList.add('new-comment-box1');
    let newCommentName = document.createElement('h3');
    newCommentName.textContent = comment.name;
    newCommentBox1.appendChild(newCommentName);
    let newCommentText = document.createElement('p');
    newCommentText.classList.add('new-comment-text');
    newCommentText.textContent = comment.text;
    newCommentBox1.appendChild(newCommentText);
    let newCommentData = document.createElement('p');
    newCommentData.classList.add('new-comment-data');
    newCommentData.textContent = formatDate(comment.data);
    newCommentBox1.appendChild(newCommentData);
    newComment.appendChild(newCommentBox1);
    let newCommentBox2 = document.createElement('div');
    newCommentBox2.classList.add('new-comment-box2');
    newCommentBox2.setAttribute('idx', id);
    let image1 = document.createElement('img');
    image1.src = './asset/70757.png';
    image1.classList.add('delete');
    image1.addEventListener('click', onDeleteClick);
    let image2 = document.createElement('img');
    image2.src = comment.like ?'./asset/heart.png' : './asset/love.png';
    image2.classList.add('like');
    image2.addEventListener('click', onLikeClick);
    newCommentBox2.appendChild(image2);
    newCommentBox2.appendChild(image1);
    newComment.appendChild(newCommentBox2);
    document.getElementById('new-comment').appendChild(newComment);
}
function render(){
    let container = document.getElementById('new-comment');
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
    let commentReverse = state.comments.slice().reverse();
    for (let idx in commentReverse){
        renderComment(commentReverse[idx], idx);
    }
}

function onDeleteClick(event){
    let index = event.target.parentNode.getAttribute('idx');
    state.comments.splice(state.comments.length - index - 1, 1);
    render();
}

function onLikeClick(event){
    let index = event.target.parentNode.getAttribute('idx');
    state.comments[state.comments.length - index - 1].like = !state.comments[state.comments.length - index - 1].like;
    render();
}
let el = document.getElementById('button');
el.addEventListener('click', onButtonClick);

function onClickEnter(event){
    if (event.code === 'Enter')
    {
        onButtonClick()
    }
}
document.getElementById('name')
    .addEventListener('keyup', onClickEnter)
document.getElementById('text')
    .addEventListener('keyup', onClickEnter)
let elName = document.getElementById('name')
let elText = document.getElementById('text')
elName.addEventListener('input', hideValidationAlert)
elText.addEventListener('input', hideValidationAlert)




